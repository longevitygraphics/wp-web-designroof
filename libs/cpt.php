<?php

    function lg_custom_post_type(){
      register_post_type( 'service',
          array(
            'labels' => array(
              'name' => __( 'Services' ),
              'singular_name' => __( 'Service' )
            ),
            'public' => true,
            'rewrite' => array(
              'with_front' => false
            ),
            'has_archive' => false,
            'menu_icon'   => 'dashicons-location',
            'show_in_menu'    => 'lg_menu',
            'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
          )
      );
    }

    add_action( 'init', 'lg_custom_post_type' );

    function lg_custom_taxonomy(){

        register_taxonomy(
          'service-category',
          'service',
          array(
            'label' => __( 'Category' ),
            'rewrite' => array( 
              'slug' => 'service-category',
              'with_front' => false
            ),
            'hierarchical' => true,
          )
        );

        add_submenu_page(
           'lg_menu',
           __('Service Category'),
           __('Service Category'),
           'edit_themes',
           'edit-tags.php?taxonomy=service-category'
        );
    }

    add_action( 'init', 'lg_custom_taxonomy' );


?>