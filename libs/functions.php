<?php

function main_nav_items ( $items, $args ) {
    //lg_write_log($args);
    if ($args->menu->slug == 'main-menu') {
        $items .= '<li class="nav-req-quote menu-item menu-item-type-custom menu-item-object-custom nav-item ml-3"><a class="nav-link btn btn-primary text-white" href="/contact">Request a Quote</a></li>';
    }
    return $items;
}
add_filter( 'wp_nav_menu_items', 'main_nav_items', 10, 2 );

add_action('wp_content_top', 'featured_banner_top', 1); // ('wp_content_top', defined function name, order)

    function featured_banner_top(){
        ob_start();
        if (!is_single() || is_singular('service')){
            get_template_part('/templates/template-parts/page/top-banner');
        }
        
        echo ob_get_clean();
    }
// get the domain plus theme url
if( !defined('THEME_IMG_PATH')){
    define( 'THEME_IMG_PATH', get_stylesheet_directory_uri());
}

class lgFunctions {

    private static $instance = null;
    /*
     * custom pagination with bootstrap .pagination class
     * source: http://www.ordinarycoder.com/paginate_links-class-ul-li-bootstrap/
     */
    function bootstrap_pagination( $echo = true ) {
        global $wp_query;

        $big = 999999999; // need an unlikely integer

        $pages = paginate_links( array(
                'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format'    => '?paged=%#%',
                'current'   => max( 1, get_query_var( 'paged' ) ),
                'total'     => $wp_query->max_num_pages,
                'type'      => 'array',
                'prev_next' => true,
                'prev_text' => __( '« Prev' ),
                'next_text' => __( 'Next »' ),
                'mid_size'  => 1,
                'end_size'  => 0
            )
        );

        if ( is_array( $pages ) ) {
            $paged = ( get_query_var( 'paged' ) == 0 ) ? 1 : get_query_var( 'paged' );

            $pagination = '<nav aria-label="Blog navigation"><ul class="pagination pagination-lg justify-content-center">';

            foreach ( $pages as $page ) {
                $pagination .= "<li class=\"page-item\">$page</li>";
            }

            $pagination .= '</ul></nav>';

            if ( $echo ) {
                echo $pagination;
            } else {
                return $pagination;
            }
        }
    }

    public static function getInstance() {
        if ( self::$instance == null ) {
            self::$instance = new lgFunctions();
        }

        return self::$instance;
    }

}

lgFunctions::getInstance();
?>