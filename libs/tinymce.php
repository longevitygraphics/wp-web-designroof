<?php

	global $lg_tinymce_custom;
	
	$lg_tinymce_custom = array(
	    'title' => 'Custom',
	    'items' =>  array(
	    	array(
				'title' => 'Underline',
	            'selector' => 'a',
	            'classes' => 'underline-text'
			),
			array(
				'title' => 'PDF',
	            'selector' => 'a',
	            'classes' => 'pdf-list-style'
			),
			array(
				'title' => 'Hide on Service Page',
	            'selector' => 'a',
	            'classes' => 'hide-on-service'
			),
			array(
				'title' => 'Font Weight 500',
	            'selector' => '.h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6, p, a',
	            'classes' => 'font-500'
			),
			array(
				'title' => 'Font Weight 600',
	            'selector' => '.h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6, p, a',
	            'classes' => 'font-600'
			),
			array(
				'title' => 'Font Weight 700',
	            'selector' => '.h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6, p, a',
	            'classes' => 'font-700'
			)

	    )
	);

?>