<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
	<?php do_action('wp_content_bottom'); ?>
	</div>
	<?php if (is_front_page()): ?>
		<footer class="bg-theme-gray">
			<div class="splash-footer-wrapper d-flex justify-content-between align-items-center py-4 px-5">
				<div><p class="text-white">Copyright 2019 Design & Detail Roofing. All Rights Reserved</p></div>
				<div class="splash-footer-social"><?php echo do_shortcode('[lg-social-media]'); ?></div>
			</div>
		</footer>
	<?php else: ?>
		<footer id="site-footer" class="bg-theme-gray text-white">
			<div class="container">
				<div class="footer-wrapper row py-5">
					<div class="col-md-6 col-lg-3">
						<div>
							<h3>Services</h3>
							<?php echo do_shortcode("[maxmegamenu location=max_mega_menu_2]"); ?>
						</div>
					</div>
					<div class="col-md-6 col-lg-3">
						<div>
							<h3>Quick Links</h3>
							<?php echo do_shortcode("[maxmegamenu location=bottom-nav]"); ?>
						</div>
					</div>
					<div class="col-md-6 col-lg-3">
						<div>
							<h3>Contact Us</h3>
							<ul>
								<li><a href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>">Emergency Phone</a></li>
								<li><a href="tel:<?php echo do_shortcode('[lg-phone-alt]'); ?>">Phone</a></li>
								<li><a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>">Email</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-6 col-lg-3">
						<div>
							<h3>Our Location</h3>
							<?php $map=get_field('map', 'options') ?>
							<?php if ($map): ?>
								<?php echo $map; ?>
							<?php endif ?>
						</div>
					</div>
				</div>
			</div>
			<?php if(!$lg_option_footer_site_legal || $lg_option_footer_site_legal == 'enable'): ?>
				<div id="site-legal" class="py-3 px-3">
					<div class="d-flex justify-content-center justify-content-md-between align-items-center flex-wrap">
						<div class="site-info"><?php get_template_part("/templates/template-parts/footer/site-info"); ?></div>
						<div class="site-longevity"> <?php get_template_part("/templates/template-parts/footer/site-footer-longevity"); ?> </div>
					</div>
				</div>
			<?php endif; ?>
		</footer><!-- #colophon -->
	<?php endif ?>
<?php wp_footer(); ?>
</body>
</html>
<?php do_action('document_end'); ?>