<?php
/**
 * Main template file
 *
 */
?>

<?php get_header(); ?>
	<main class="single-blog-main">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="container">
				<h1><?php the_title(); ?></h1>
				<p class="text-uppercase my-3"><i class="far fa-clock mr-1"></i><?php the_date(); ?> <i class="far fa-user-circle mr-1 ml-2"></i> <?php the_author_link(); ?></p>
				<?php if (is_singular('post')): ?>
					<div class="blog-share">
						<?php echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]'); ?>
					</div>
				<?php endif ?>
			</div>
			<div class="container">
				<?php the_content(); ?>
			</div>
		<?php endwhile; endif; ?>
		<?php  
				$prev_post = get_previous_post();
				$next_post = get_next_post(); 
			?>
				<div class="mt-4 single-post-pagination container d-flex align-items-center flex-wrap justify-content-between">
					<?php if (!empty($prev_post)): ?>
						<a class="py-1" href="<?php echo get_permalink( $prev_post->ID ); ?>">« Prev</a>
					<?php endif ?>	
					<?php if (!empty($next_post)): ?>
						<a class="py-1" href="<?php echo get_permalink( $next_post->ID ); ?>">Next »</a>
					<?php endif ?>
				</div>
	</main>
<?php get_footer(); ?>