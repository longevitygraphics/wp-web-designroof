<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>
	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>

				<div class="container py-4">

					<h2 class="h2 text-center mb-4"><strong>Gallery</strong></h2>

					<?php if (have_rows("gallery_images")) : ?>
						<div class="gallery-grid row">
							<?php while(have_rows("gallery_images")) : the_row(); ?>
								<?php 
									$lightbox_img = get_sub_field("gallery_image_lightbox");
									$gallery_content = get_sub_field("gallery_content");
								 ?>
								<div class="grid-item col-sm-6 col-md-4 mb-3">
									<div>
							            <div class="gallery-img">
							            	<img src="<?php echo $lightbox_img['url']; ?>" alt="<?php echo $lightbox_img['alt']; ?>">
							            </div>
							            <a class="gallery-content" data-lightbox="roadtrip" href="<?php echo $lightbox_img['url']; ?>">
							            	<?php echo $gallery_content; ?>	
							            </a>
						            </div>
						        </div>
							<?php endwhile; ?>
						</div>
					<?php endif ?>
				</div>

			</main>
		</div>
	</div>
<?php get_footer(); ?>