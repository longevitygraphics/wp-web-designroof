<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<main>

		<?php flexible_layout(); ?>
		<?php
			$term = get_queried_object();
			$service_terms = get_terms( 'service-category', array(
			    'hide_empty' => false,
			) );
			$serv_short_desc = get_field("short_description", $term);
		?>
		<?php if ($service_terms && is_array($service_terms) && sizeof($service_terms) > 0): ?>
		<?php 
			$index = floor(sizeof($service_terms) / 2);
		 	$array_left = array_slice($service_terms, 0, $index);
		 	$array_right = array_slice($service_terms, $index);
		 	$i = 0;
		 ?>
		<div class="row m-0 justify-content-between mt-3">
			<div class="info-accordian col-lg-5">
				<?php if($array_left && is_array($array_left)): ?>
					<?php foreach ($array_left as $key => $value):?>
						<?php $serv_short_desc = get_field("short_description", 'service-category_' . $value->term_id); ?>
					 	<div class="single-accordian">
							<a class="btn trigger-collapse" data-toggle="collapse" data-target="#service-info-<?php echo $i; ?>" aria-expanded="false" aria-controls="service-info-<?php echo $i; ?>">
								<h3 class='text-dark'><?php echo $value->name; ?></h3>
								<i class="fas fa-chevron-down"></i>
							</a>
							<div class="collapse" id="service-info-<?php echo $i; ?>">
								<?php echo $serv_short_desc; ?>
							</div>
						</div>
					<?php $i++; endforeach; ?>
				<?php endif; ?>
			</div>
			<div class="info-accordian col-lg-5">
				<?php if($array_right && is_array($array_right)): ?>
					<?php foreach ($array_right as $key => $value):?>
						<?php $serv_short_desc = get_field("short_description", 'service-category_' . $value->term_id); ?>
					 	<div class="single-accordian">
							<a class="btn trigger-collapse" data-toggle="collapse" data-target="#service-info-<?php echo $i; ?>" aria-expanded="false" aria-controls="service-info-<?php echo $i; ?>">
								<h3 class='text-dark'><?php echo $value->name; ?></h3>
								<i class="fas fa-chevron-down"></i>
							</a>
							<div class="collapse" id="service-info-<?php echo $i; ?>">
								<?php echo $value->name; ?>
							</div>
						</div>
					<?php $i++; endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
		<?php endif ?>

	</main>

<?php get_footer(); ?>