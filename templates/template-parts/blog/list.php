<?php if ( have_posts() ) : ?>
	<div class="blog_list_small">
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="row no-gutters pb-3">
		<?php if(has_post_thumbnail()): ?>
			<div class="col-md-5">
				<img src="<?php echo get_the_post_thumbnail_url(); ?>">
			</div>
			<div class="col-md-7 px-md-5 py-4 py-md-0">
				<header>
					<h2><a class="text-dark" href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
					<p class="small text-uppercase my-3"><i class="far fa-clock mr-1"></i><?php the_date(); ?> <i class="far fa-user-circle mr-1 ml-2"></i> <?php the_author_link(); ?></p>
				</header>
				<div class="article-content">
					<p><?php the_excerpt(); ?></p>
					<a class="d-inline-block mt-4 text-secondary arrow-link link--arrowed" href="<?php echo get_permalink(); ?>">CONTINUE READING<?php include 'arrow.svg'; ?></i></a>
				</div>
			</div>
		<?php else: ?>
			<div class="col-12">
				<header>
					<h2><a class="text-dark" href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
					<p class="small text-uppercase my-3"><i class="far fa-clock mr-1"></i><?php the_date(); ?> <i class="far fa-user-circle mr-1 ml-2"></i> <?php the_author_link(); ?></p>
				</header>
				<div class="article-content">
					<p><?php the_excerpt(); ?></p>
				</div>
				<a class="d-inline-block mt-4 text-secondary arrow-link link--arrowed" href="<?php echo get_permalink(); ?>">CONTINUE READING<?php include 'arrow.svg'; ?></i></a>
			</div>
		<?php endif; ?>

		<?php if (($wp_query->current_post +1) != ($wp_query->post_count)): ?>
			<hr class="lg d-none d-md-block">
		<?php endif; ?>
		</div>
	<?php endwhile; ?>
	</div>
<?php endif ?>