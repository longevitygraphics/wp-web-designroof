<?php if( is_front_page() ) : ?>
	<?php 
		$first_roofing_banner = get_field('first_roofing_top_banner');
		$first_roofing_overlay = get_field('first_roofing_overlay');
		$second_roof_banner = get_field('second_roof_top_banner');
		$second_roof_overlay = get_field('second_roof_overlay');
		$first_roofing_logo = get_field('first_roofing_logo');
        $first_roofing_office = get_field('first_roofing_office');                      
        $first_roofing_emergency= get_field('first_roofing_emergency');
        $second_roof_logo = get_field('second_roof_logo');
        $second_roof_office = get_field('second_roof_office');
        $second_roof_emergency= get_field('second_roof_emergency');
	 ?>
	<div class="splash-top-banner">
		<div class="d-flex splash-banner-wrappers">
			<?php if ($first_roofing_banner): ?>
				<div class="splash-top-banner-image position-relative">
					<img src="<?php echo $first_roofing_banner['url']; ?>" alt="<?php echo $first_roofing_banner['alt']; ?>">
					<?php if ($first_roofing_overlay): ?>
						<div class="splash-overlay first-roofing-overlay position-absolute">
							<?php if ($first_roofing_logo): ?>
								<div><img class="splash-mobile-logo" src="<?php echo $first_roofing_logo['url']; ?>" alt="<?php echo $first_roofing_logo['alt']; ?>"></div>
							<?php endif ?>
							<?php echo $first_roofing_overlay; ?>
						</div>
					<?php endif ?>
					<div class="splash-first-overlay-phone position-absolute d-flex align-items-center justify-content-between p-3 text-white">
						<?php if ($first_roofing_office): ?>
							<p class="m-0">Phone: <a class="text-white" href="tel:<?php echo $first_roofing_office; ?>"><?php echo $first_roofing_office; ?></a></p>
						<?php endif ?>
						<?php if ($first_roofing_emergency): ?>
							<p class="m-0">Emergency Phone Number: <a class="text-white" href="tel:<?php echo $first_roofing_emergency; ?>"><?php echo $first_roofing_emergency; ?></a></p>
						<?php endif ?>
					</div>
				</div>
			<?php endif; ?>
			<?php if ($second_roof_banner): ?>
				<div class="splash-top-banner-image position-relative">
					<img src="<?php echo $second_roof_banner['url']; ?>" alt="<?php echo $second_roof_banner['alt']; ?>">
					<?php if ($second_roof_overlay): ?>
						<div class="splash-overlay second-roof-overlay position-absolute">
							<?php if ($second_roof_logo): ?>
								<div><img class="splash-mobile-logo" src="<?php echo $second_roof_logo['url']; ?>" alt="<?php echo $second_roof_logo['alt']; ?>"></div>
							<?php endif ?>
							<?php echo $second_roof_overlay; ?>
						</div>
					<?php endif ?>
					<div class="splash-second-overlay-phone position-absolute d-flex align-items-center justify-content-between p-3 text-white">
						<?php if ($second_roof_office): ?>
							<p class="m-0">Phone: <a class="text-white" href="tel:<?php echo $second_roof_emergency; ?>"><?php echo $second_roof_office; ?></a></p>
						<?php endif ?>
						<?php if ($second_roof_emergency): ?>
							<p class="m-0">Emergency Phone Number: <a class="text-white" href="tel:<?php echo $second_roof_emergency; ?>"><?php echo $second_roof_emergency; ?></a></p>
						<?php endif ?>
					</div>
				</div>
			<?php endif; ?>
			
		</div>
	</div>
	<?php elseif(is_tax($taxonomy = 'service-category') || is_archive()) : ?>
		<div class="top-banner">
		<?php $term = get_queried_object(); ?>
		<?php 
			$default_banner = get_field("default_banner","options");
			$default_banner = get_field("default_banner","options")['gallery'];
			$gallery = 	$default_banner;
			$default_overlay = get_field("default_overlay","options");
			$use_default_banner = get_field("use_default_banner", $term);
		?>
			<?php if (is_archive() || ($default_banner && $use_default_banner)): ?>
				 <?php include(locate_template('/layouts/components/gallery.php')); ?>
				 	<?php if($default_overlay) : ?>
						<div class='top-banner-overlay'>
							<div class='banner-text container'>
								<?php echo $default_overlay; ?>
							</div>
						</div>
					<?php endif; ?>
				 
			<?php else: ?>

			<?php 
				$top_banner_images = get_field("top_banner", $term)['gallery'];
				$gallery = 	$top_banner_images;
				$text_overlay = get_field("text_overlay", $term); 
			?>
			<?php if($top_banner_images) : ?>
				<?php include(locate_template('/layouts/components/gallery.php')); ?>
					<?php if($text_overlay) : ?>
						<?php 
							echo "<div class='top-banner-overlay'>";
							echo "<div class='banner-text container'>";
							echo $text_overlay;
							echo "</div>"; //end of banner text
							echo "</div>"; // end of top banner overlay
						?>
					<?php endif; ?>
				
			<?php endif; ?>
		<?php endif ?>
	</div>
	<?php else : ?>
	<div id="<?php if(is_page('home')){echo 'home-top-banner';} ?>" class="top-banner">
		<?php
			$term = get_queried_object();
			$blog_use_default_banner = get_field("use_default_banner", $term);
			$default_banner = get_field("default_banner","options");
			$default_banner = get_field("default_banner","options")['gallery'];
			$gallery = 	$default_banner;
			$default_overlay = get_field("default_overlay","options");
			$use_default_banner = get_field("use_default_banner");
		?>
			<?php if (($default_banner && $use_default_banner) || ($default_banner && $blog_use_default_banner)): ?>
				 <?php include(locate_template('/layouts/components/gallery.php')); ?>
					 <?php if($default_overlay) : ?>
						<div class='top-banner-overlay'>
							<div class='banner-text container'>
								<?php echo $default_overlay; ?>
							</div>
						</div>
					<?php endif; ?>
			<?php else: ?>
			<?php
				if (is_home()) {
				 	$top_banner_images = get_field('top_banner', $term);
					$top_banner_images = get_field("top_banner", $term)['gallery'];
					$gallery = 	$top_banner_images;
					$text_overlay = get_field("text_overlay", $term);
				 } else {
				 	$top_banner_images = get_field('top_banner');
					$top_banner_images = get_field("top_banner")['gallery'];
					$gallery = 	$top_banner_images;
					$text_overlay = get_field("text_overlay");
				 }
			?>
			<?php if($top_banner_images) : ?>
				<?php include(locate_template('/layouts/components/gallery.php')); ?>
				<?php if($text_overlay) : ?>
				<div id="<?php if(is_page('home')){echo 'home-top-banner-overlay';} ?>" class='top-banner-overlay'>
					<div class='banner-text container'>
						<?php echo $text_overlay; ?>
					</div>
				</div>
				<?php endif; ?>
			<?php endif; ?>
		<?php endif ?>
	</div>
<?php endif; ?>