<?php if(have_rows("why_us", "options")) : ?>
	<div class="why-us d-flex flex-wrap row">
		<?php while (have_rows("why_us", "options")) : the_row(); ?>
		<?php 
			$why_us_title = get_sub_field("why_us_title", "options");
			$why_us_content = get_sub_field("why_us_content", "options");
		 ?>
		 <div class=" col-sm-6 col-md-4 col-lg-3 mb-3">
		 	<div class="mx-md-3 card py-5 px-4 d-flex flex-column">
			 	<?php if($why_us_title) : ?>
					<h3 class="h6"><?php echo $why_us_title; ?></h3>
				<?php endif; ?>
				<?php if($why_us_content) : ?>
					<?php echo $why_us_content; ?>
				<?php endif; ?>	
			</div>	 	  
		 </div>
		<?php endwhile; ?>
	</div>
<?php endif; ?>