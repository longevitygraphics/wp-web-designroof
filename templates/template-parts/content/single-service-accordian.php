 <?php 
	$terms = get_the_terms( get_the_ID(), 'service-category' );
	if ($terms && ! is_wp_error( $terms )) {
		$service_categories = array();
			foreach ( $terms as $term ) {
				$service_categories[] = $term->name;
			}               
			$on_cats = join( ", ", $service_categories );
	}	
	$args = array(
		'post_type' => 'service',
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'tax_query' => array(
			 array(
				'taxonomy' => 'service-category',
				'field' => 'slug',
				'terms' => 'featured-service'
			)
		)
	); 
	$featured_service_query = new WP_Query( $args );   ?>

	<?php if ($featured_service_query->have_posts() && is_array($featured_service_query->posts) && sizeof($featured_service_query->posts) > 0 ): 
			$index = ceil(sizeof($featured_service_query->posts) / 2);
		 	$array_left = array_slice($featured_service_query->posts, 0, $index);
		 	$array_right = array_slice($featured_service_query->posts, $index);
		 	$i = 0;
	?>
	<div class="row m-0 justify-content-between mt-3">
		<div class="info-accordian col-lg-5">
			<?php if($array_left && is_array($array_left)): ?>
				<?php foreach ($array_left as $key => $value): ?>
					<?php $featured_service_query->the_post(); ?>
					<div class="single-accordian">
						<a class="btn trigger-collapse" data-toggle="collapse" data-target="#service-info-<?php echo $i; ?>" aria-expanded="false" aria-controls="service-info-<?php echo $i; ?>">
							<h3 class='text-dark'><?php echo get_the_title(); ?></h3>
							<i class="fas fa-chevron-down"></i>
						</a>
						<div class="collapse pb-4" id="service-info-<?php echo $i; ?>">
							<?php $service_short_description = get_field("featured_service_short_description"); ?>
								<?php if ($service_short_description): ?>
									<?php echo $service_short_description; ?>
									<p class="mt-3" ><a href="<?php echo get_the_permalink(); ?>">Read More</a></p>
								<?php endif ?>
						</div>
					</div>
				<?php $i++; endforeach ?>
			<?php endif?>
		</div>
		<div class="info-accordian col-lg-5">
			<?php if($array_right && is_array($array_right)): ?>
				<?php foreach ($array_right as $key => $value): ?>
					<?php $featured_service_query->the_post(); ?>
					<div class="single-accordian">
						<a class="btn trigger-collapse" data-toggle="collapse" data-target="#service-info-<?php echo $i; ?>" aria-expanded="false" aria-controls="service-info-<?php echo $i; ?>">
							<h3 class='text-dark'><?php echo get_the_title(); ?></h3>
							<i class="fas fa-chevron-down"></i>
						</a>
						<div class="collapse pb-4" id="service-info-<?php echo $i; ?>">
							<?php $service_short_description = get_field("featured_service_short_description"); ?>
								<?php if ($service_short_description): ?>
									<?php echo $service_short_description; ?>
									<p class="mt-3" ><a href="<?php echo get_the_permalink(); ?>">Read More</a></p>
								<?php endif ?>
						</div>
					</div>
				<?php $i++; endforeach ?>
			<?php endif?>
		</div>
	</div>
	<?php endif ?>
	<?php wp_reset_postdata(); // reset the query ?>

