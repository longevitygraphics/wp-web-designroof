<?php get_header(); ?>
	<main>
		<?php 
			$single_service_description = get_field('single_service_description');
			$short_description = get_field("short_description", $term);
			$short_description_image = get_field("short_description_image", $term);
		 ?>
		<?php if ($short_description_image && $short_description_image): ?>
			<section class="service-short-description container my-5">
			 	<div class="short-description-wrapper row justify-content-between align-items-stretch">
				 	<div class="short-description col-md-6 mb-3 mb-md-0">
				 		<?php echo $short_description; ?>
				 	</div>
				 	<div class="short-description-image col-md-6">
				 		<img class="w-100" src="<?php echo $short_description_image['url']; ?>" alt="<?php echo $short_description_image['alt']; ?>">
				 	</div>
				 </div>
			</section>
		<?php else: ?>
			<section class="single-service-intro py-5">
				<div class="single-service-intro-wrapper container">
					<?php  echo get_the_content(); ?>
				</div>
			</section>
		<?php endif ?>
		<?php 
			$single_service_details_title = get_field("single_service_details_title");
		 ?>
<!-- 		<section class="single-service-details-section bg-lighter-gray py-5">
			<div class="single-service-details-wrapper">
				<div class="single-service-details-title mb-5">
					<?php echo $single_service_details_title; ?>
				</div>
				<div>
					<?php if(have_rows("single_service_details")) : ?>
						<div class="container">
							<div class="single-service-details row">
								<?php while(have_rows("single_service_details")) : the_row(); ?>
									<?php 
										$single_service_image = get_sub_field("single_service_detail_image");
										$single_service_content = get_sub_field("single_service_detail_content");
									?>
									<div class="single-service-detailed col-md-6 col-lg-3">
										<div class="single-service-detailed-image">
											<img src="<?php echo $single_service_image['url']; ?>" alt="<?php echo $single_service_image['alt']; ?>">
										</div>
										<div class="single-service-detailed-content">
											<?php echo $single_service_content; ?>
										</div>
									</div>
								<?php endwhile; ?>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</section> -->
		<section class="py-4 bg-light">
			<div class="container">
				<?php $featured_service_title = get_field("featured_service_title", "options") ?>
				<?php if ($featured_service_title): ?>
					<?php echo $featured_service_title; ?>
				<?php endif ?>
		 		<?php get_template_part('/templates/template-parts/content/single-service-accordian'); ?>
		 	</div>
		</section>
		<section class="pt-5" id="service-form-section"> <!-- Form Section -->
		 	<?php 
		 		$ready_to_start = get_field('ready_to_start', 'option');
		 		$ready_to_form = get_field('ready_to_start_form', 'option');
		 		$form_title = get_field('form_title', 'option');
		 	 ?>
		 	<div class="container">
			 	<div class="service-form-wrapper row ">
			 		<div class="col-md-6"><?php echo $ready_to_start; ?></div>
			 		<div class="pt-3 pt-md-0 service-form col-md-6">
			 			<?php echo $form_title; ?>
			 			<?php echo do_shortcode("[gravityform id=1 title=false description=false ajax=true tabindex=49]"); ?>
			 		</div>
			 	</div>
		 	</div>
		 </section>
		 
	</main>
<?php get_footer(); ?>