<?php

get_header(); ?>

	<main>
		<?php
			$term = get_queried_object();
		?>
		<?php 
			// short description section
			$short_description = get_field("short_description", $term);
			$short_description_extra = get_field("short_description_extra", $term);
			$short_description_image = get_field("short_description_image", $term);
		 ?>
		 <section class="service-short-description container my-5">
		 	<div class="short-description-wrapper row justify-content-between align-items-stretch">
			 	<div class="short-description col-md-6">
			 		<?php echo $short_description; ?>
			 	</div>
			 	<div class="short-description-image col-md-6">
			 		<img class="w-100" src="<?php echo $short_description_image['url']; ?>" alt="<?php echo $short_description_image['alt']; ?>">
			 	</div>
			 </div>
			 <?php if ($short_description_extra): ?>
			 	<div class="short-description-extra pt-3">
			 		<?php echo $short_description_extra; ?>
			 	</div>
			 <?php endif; ?>
		 </section>
		 <section class="py-5 bg-light px-md-5"> <!-- Category information -->
		 	<div class="service-category-info-wrapper">
				 <?php if(have_rows("category_information", $term)) : ?>
				 	<div class="service-category-info row">
				 		<?php while(have_rows("category_information", $term)) : the_row(); ?>
				 		<?php  
				 			$info_image = get_sub_field("info_image", $term);
				 			$info_content = get_sub_field("info_content", $term);
				 		?>
				 		<div class="key-infos col-md-4 mb-4 mb-md-0">
				 			<div class="key-info-image">
				 				<img src="<?php echo $info_image['url']; ?>" alt="<?php echo $info_image['alt']; ?>">
				 			</div>
				 			<div class="key-info-conetnt mt-3">
				 				<?php echo $info_content; ?>
				 			</div>
				 		</div>
				 		<?php endwhile; ?>
				 	</div>
				 <?php endif; ?>
			 </div>
		 </section>
		<section class="py-5">
			<div class="container">
				<?php $featured_service_title = get_field("featured_service_title", "options") ?>
				<?php if ($featured_service_title): ?>
					<?php echo $featured_service_title; ?>
				<?php endif ?>
		 		<?php get_template_part('/templates/template-parts/content/service-cat-accordian'); ?>
		 	</div>
		</section>
		<section class="pt-4" id="service-form-section"> <!-- Form Section -->
		 	<?php 
		 		$ready_to_start = get_field('ready_to_start', 'option');
		 		$ready_to_form = get_field('ready_to_start_form', 'option');
		 		$form_title = get_field('form_title', 'option');
		 	 ?>
		 	<div class="container">
			 	<div class="service-form-wrapper row ">
			 		<div class="col-md-6"><?php echo $ready_to_start; ?></div>
			 		<div class="pt-3 pt-md-0 service-form col-md-6">
			 			<?php echo $form_title; ?>
			 			<?php echo do_shortcode("[gravityform id=1 title=false description=false ajax=true tabindex=49]"); ?>
			 		</div>
			 	</div>
		 	</div>
		</section>
	</main>
<?php get_footer(); ?>