// Windows Ready Handler
 	
(function($) {
    $(document).ready(function(){
    	console.log('231231');
    	let a = 'aaa';
    	console.log(a);
        //menu parent clickable
        if ($(window).width() > 991) {
            $('.navbar #main-navbar .dropdown').hover(function() {
                $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
            }, function() {
                $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();
            });

            $('.navbar #main-navbar .dropdown > a').click(function(){
                location.href = this.href;
            });
        } else {
            var timesClicked = 0;
            
            $('.navbar #mobile-navbar .nav-link').click(function(){
                timesClicked++;
                console.log(timesClicked);
                if(timesClicked==1){
                    $('.navbar #mobile-navbar .dropdown').hover(function() {
                        $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
                    }, function() {
                        $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();
                    });

                    $('.navbar #mobile-navbar .dropdown > a').click(function(){
                        location.href = this.href;
                    });
                        }
                    })

        }
        
        //end menu parent clickable

        // scroll to this id

        //resource page
        $(".interactive-titles a").click(function(){
            setTimeout(function(){ 
                $(".interactive-overlay").addClass('active-interactive-overlay');
            }, 200);
        });

        $("#svg-container .nav-pills a").click(function(){
            setTimeout(function(){ 
                $(".interactive-overlay").addClass('active-interactive-overlay');
            }, 200);
        });

        $(".close-overlay").click(function(){
            $(".interactive-overlay").removeClass('active-interactive-overlay');
        });

        function getUrlVars() {
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                vars[key] = value;
            });
            return vars;
        }
            
        $(document).ready(function(){
            var para = getUrlVars()["slideto"];
            var element = $('#'+para);
            var settings = {
                duration: 2 * 1000,
                offset: -80
            };
            if(para && element[0]){
                var scrollTo = element;
                KCollection.headerScrollTo(scrollTo, settings, function(){
                    console.log('here now');
                    // if(!element.hasClass('service-animated')){
                    //     element.addClass('animated shake active');
                    // }
                });
            }  
        });
        //end of scroll to this section

        $('.why-join .collapse-item > h2').click(function(){
            $(this).next('div').toggleClass('show-collapse-item');
        })

        $('.office-job-title').click(function(){
            $(this).next('.office-job-content').toggleClass('show-office-content');
            $(this).children('.fas').toggleClass('rotate-arrow');
        })

        $('.home').parent('html').addClass('home-html');

        //remove thank you page top banner
        if($('.thank-you').siblings('.top-banner').length !== 0){
            $(".top-banner").css('display', 'none');
        }
        
    });
}(jQuery));


