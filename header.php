<?php
/**
 * The header for our theme
 *
 */

?>

<?php do_action('document_start'); ?>

<!doctype html>
<html <?php language_attributes(); ?> <?php do_action('html_class'); ?>>
<head>
  <!--[if lt IE 9]>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
  <![endif]-->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel='preload' href="<?php echo THEME_IMG_PATH; ?>/assets/dist/fonts/fa-solid-900.woff2" as='font' type='font/woff2' crossorigin="anonymous">
  <link rel='preload' href="<?php echo THEME_IMG_PATH; ?>/assets/dist/fonts/fa-regular-400.woff2" as='font' type='font/woff2' crossorigin="anonymous">
  <link rel='preload' href="<?php echo THEME_IMG_PATH; ?>/assets/dist/fonts/fa-brands-400.woff2" as='font' type='font/woff2' crossorigin="anonymous">
  <?php do_action('wp_header'); ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <?php do_action('wp_body_start'); ?>
  <div id="page" class="site">
    <?php if(is_front_page()) :  ?>
      <header class="splash-header">
          <div class="bg-theme-gray py-3">
              <div>
                <?php 
                    $first_roofing_logo = get_field('first_roofing_logo');
                    $first_roofing_office = get_field('first_roofing_office');
                    $first_roofing_link = get_field('first_roofing_link');
                    if ($first_roofing_link) {
                      $first_roofing_link_url = $first_roofing_link['url'];
                      $first_roofing_link_title = $first_roofing_link['title'];
                      $first_roofing_link_target = $first_roofing_link['target'] ? $first_roofing_link['target'] : '_self';
                    }
                    $first_roofing_emergency= get_field('first_roofing_emergency');
                    $second_roof_logo = get_field('second_roof_logo');
                    $second_roof_office = get_field('second_roof_office');
                    $second_roofing_link = get_field('second_roofing_link');
                    if ($second_roofing_link) {
                      $second_roofing_link_url = $second_roofing_link['url'];
                      $second_roofing_link_title = $second_roofing_link['title'];
                      $second_roofing_link_target = $second_roofing_link['target'] ? $first_roofing_link['target'] : '_self';
                    }
                    $second_roof_emergency= get_field('second_roof_emergency');
                ?>
                  <div class="d-md-flex justify-content-between splash-header text-white">
                      <div class="nav-first-roofing">
                          <div class=" d-flex align-items-center justify-content-center">
                              <div>
                                  <a href="<?php echo esc_url( $first_roofing_link_url); ?>" target="<?php echo esc_attr($first_roofing_link_target); ?>"><img class="pr-3" src="<?php echo $first_roofing_logo['url']; ?>" alt="<?php echo $first_roofing_logo['alt']; ?>"></a>
                              </div>
                              <div class="pl-3 border-left border-white">
                                  <p>Office: <a class="text-white" href="tel:<?php echo $first_roofing_office; ?>"><?php echo $first_roofing_office; ?></a></p>
                                  <p>After Hours Emergency: <a class="text-white" href="tel:<?php echo $first_roofing_emergency ?>"><?php echo $first_roofing_emergency; ?></a></p>
                              </div>
                          </div>
                      </div>
                      <div class="nav-second-roof">
                          <div class="d-flex align-items-center justify-content-center">
                              <div>
                                  <a href="<?php echo esc_url( $second_roofing_link_url); ?>" target="<?php echo esc_attr($second_roofing_link_target); ?>"><img class="pr-3" src="<?php echo $second_roof_logo['url']; ?>" alt="<?php echo $second_roof_logo['alt']; ?>"></a>
                              </div>
                              <div class="pl-3 border-left border-white">
                                    <p>Office: <a class="text-white" href="tel:<?php echo $second_roof_office; ?>"><?php echo $second_roof_office; ?></a></p>
                                    <p>After Hours Emergency: <a class="text-white" href="tel:<?php echo $second_roof_emergency; ?>"><?php echo $second_roof_emergency; ?></a></p>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </header>
    <?php else : ?>
    	<header id="site-header">
    		<div class="utility-bar bg-theme-light-gray py-0">
    			<div class="d-flex justify-content-between align-items-center flex-wrap">
    				<div class="emergency-repair text-white bg-danger px-3 py-2">
    					<p>After Hours Emergency Repairs: <a href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo do_shortcode("[lg-phone-main]"); ?></a> or <a class="text-white" href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><u>Email Us</u></a></p>
    				</div>
    				<div class="utility-menu">
    					<?php echo do_shortcode("[maxmegamenu location=max_mega_menu_1]"); ?>
    					<div class="header-social-media">
  	          			<?php echo do_shortcode("[lg-social-media]"); ?>
  	          		</div>
  			        <div class="header-to-detail px-3 py-2 bg-theme-gray">
                  <?php  
                    $other_site = get_field("other_site", "options");
                    $other_site_link = get_field('other_site_link', "options");
                    if($other_site_link) :
                      $link_url = $other_site_link['url'];
                      $link_title = $other_site_link['title'];
                      $link_target = $other_site_link['target'] ? $other_site_link['target'] : '_self';
                  ?>
  			          	<p><a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a></p>
                  <?php endif ?>
  			        </div>
    				</div>
    			</div>
    		</div>
        <div class="mobile-utility-bar d-lg-none">
            <div class="d-flex justify-content-between align-items-center">
                <a class="btn btn-danger px-3 py-2" href="tel:<?php echo do_shortcode("[lg-phone-main]"); ?>">
                  EMERGENCY
                </a>
                <div>
                     <a class="px-3 py-2" href="tel:<?php echo do_shortcode("[lg-phone-alt]"); ?>"><i class="fas fa-phone"></i> Toll-Free</a>
                </div>
            </div>
        </div>
        <div class="header-main bg-theme-gray">
  	        <div class="d-flex justify-content-between align-items-center flex-wrap">
  	          	<div class="site-branding px-3 py-4 py-md-2">
  		            <div class="logo">
  		              <a href="/home"><?php echo site_logo(); ?></a>
  		            </div>
  		            <div class="mobile-toggle"><i class="fa fa-bars" aria-hidden="true"></i></div>
  	         	   </div><!-- .site-branding -->
    	          <?php get_template_part("/templates/template-parts/header/main-nav"); ?>
                <?php get_template_part("/templates/template-parts/header/mobile-nav"); ?>
  	         </div>
        </div>
      </header><!-- #masthead -->
    <?php endif; //end of if it is front page ?>

    <div id="site-content" role="main">
    <?php do_action('wp_content_top'); ?>