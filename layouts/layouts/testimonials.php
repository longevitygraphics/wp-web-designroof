<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<div class="d-flex flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-12">
			<?php if (have_rows("testimonial_content")): ?>
				<div class="testimonials-wapper">
					<?php while(have_rows("testimonial_content")): the_row(); ?>
					<div class="testimonial-content mb-3">
					<?php  
						$content = get_sub_field("content");
					?>
						<div class="card p-4">
							<?php echo $content; ?>
						</div>
					</div>
					<?php endwhile ?>
				</div>
			<?php endif ?>
		</div>
	</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>