<?php
	get_template_part('/layouts/partials/block-settings-start');
?>

<!--------------------------------------------------------------------------------------------------------------------------------->
	
	<div class="areas-of-expertise-section d-flex flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-12 px-0">
			<?php
			$terms = get_terms( 'service-category', array(
			    'hide_empty' => false,
			) );
			?>
			<?php if($terms) : ?>
			<div id="home-services">
				<div class="service-tablist-wrapper">
					<ul class="nav nav-tabs home-services-titles d-flex text-center justify-content-between mt-3 mb-4" role="tablist">
						<?php foreach ($terms as $key => $term): ?>
							<?php if ($term->slug!=="featured-service") : ?>
								<li class="home-service-title flex-fill bg-light">
									<a class="px-3 py-2 <?php if($key==0) {
										echo 'active';
									}?>" data-toggle="tab" role="tab" href="<?php echo '#home-service-' . $key; ?>"><?php echo $term->name; ?></a>
								</li>
							<?php endif ?>
						<?php endforeach; ?>
					</ul>
				</div>
				<div class="home-services-contents tab-content">
					<?php foreach ($terms as $key => $term): ?>
						<div id="<?php echo 'home-service-' . $key; ?>" role="tabpanel" class="tab-pane fade home-service-content<?php if($key==0) {
								echo ' active show';
							}?>">
							<div class="row justify-content-between align-items-stretch">
								<?php 
								$short_description = get_field("short_description", 'service-category_' . $term->term_id); 
								$short_image = get_field("short_description_image", 'service-category_' . $term->term_id);
								?>
								<div class="col-md-6"><?php echo $short_description; ?></div>
								<?php if ($short_image): ?>
								<div class="col-md-6 home-short-image"><img src="<?php echo $short_image['url']; ?>" alt="<?php echo $short_image['alt']; ?>"></div>
								<?php endif ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<?php endif; ?>
			<?php wp_reset_query(); ?>
		</div>
	</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 
	get_template_part('/layouts/partials/block-settings-end');
?>