<?php
	get_template_part('/layouts/partials/block-settings-start');
?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<div class="d-flex flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-12">
			<div class="benefits">
				<?php 
					$benefit_intro = get_sub_field('benefit_intro');
				?>
					<div class="benefit-intro">
						<?php echo $benefit_intro; ?>
					</div>
				<?php if(have_rows('benefit')) : ?>   
					<div class="benefit-descriptions row mt-5">
					<?php while(have_rows('benefit')) : the_row(); ?>
						<?php 
							$benefit_image = get_sub_field('benefit_image');
							$benefit_content = get_sub_field('benefit_content');
						 ?>
						<div class="company-benefit mb-5 col-md-6 col-lg-4 col-xl-3">
							<div class="company-benefit-image">
								<img class="d-block m-auto" src="<?php echo $benefit_image['url']; ?>" alt="<?php echo $benefit_image['alt']; ?>">
							</div>
							<div class="company-benefit-content mt-3">
								<?php echo $benefit_content; ?>
							</div>
						</div>
					<?php endwhile; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 
	get_template_part('/layouts/partials/block-settings-end');
?>