<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<div class="d-flex flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-12">
		<?php 
			$roofers_title = get_field('roofers_title','option');
			$roofers_content = get_field('roofers_content', 'option');
			$current_openings_message = get_sub_field('current_openings_message');
		 ?>
			 <div class="current-openings">
			 	<div class="current-openings-message">
					<?php echo $current_openings_message ?>
			 	</div>
			 	<div id="roofing-jobs" class="roofer-openings-wrapper pt-3">
				 	<div class="roofers">
				 		<div class="roofers-title title-bottom-border ">
				 			<h3 class="mb-0"><?php echo $roofers_title; ?></h3>
				 		</div>
				 		<div class="roofers-content pt-3">
				 			<?php echo $roofers_content; ?>
				 		</div>
				 	</div>
			 	</div>
		 	</div>
		</div>
	</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>