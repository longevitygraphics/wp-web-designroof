<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<div class="d-flex flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-12">
		<?php if (have_rows("interactive")): ?>
			<?php 
				$i = 0;
			 ?>
			<ul class="interactive-titles d-flex flex-wrap nav nav-pills">
			<?php while(have_rows("interactive")) : the_row(); ?>
				<?php ++$i; $title = get_sub_field("title");?>
				<li class="pr-3"><span><?php echo $i.'.'; ?> </span><a data-toggle="pill" href="<?php echo '#c'. $i; ?>"><?php echo $title; ?></a></li>
			<?php endwhile; $i=0; ?>
			</ul>
			<div id="interactive-container" class="interactive-image">
				<div id="svg-container">
					<svg class="svg-element nav nav-pills" viewBox="0 0 1200 506" version="1.1" preserveAspectRatio="xMinYMin meet" style="opacity: 1;">
					<?php $interactive_image = get_sub_field("interactive_image"); ?>
					<image href="<?php echo $interactive_image['url']; ?>" x="0" y="0" width="1200" height="506" background="black"></image>
					<?php while(have_rows("interactive")) : the_row(); ?>
						<?php 
							++$i; 
							$cx = get_sub_field("cx");
							$cy = get_sub_field("cy");
							$textx = get_sub_field("textx");
							$texty = get_sub_field("texty"); 
						?>
						<a data-toggle="pill" href="<?php echo '#c'. $i; ?>">
							<circle cx="<?php echo $cx; ?>" cy="<?php echo $cy; ?>" r="15" stroke-width="4" style="fill: white; stroke: black;"></circle>
							<text x="<?php echo $textx; ?>" y="<?php echo $texty; ?>" text-anchor="middle" fill="black" font-size="18px" dy=".35em"><?php echo $i; ?></text>
						</a>
						

					<?php endwhile; $i=0; ?>
					<div class="tab-content">
						<?php while(have_rows("interactive")) : the_row(); ?>
							<?php 
								$i++;
								$image = get_sub_field("image");
								$title = get_sub_field("title");
								$content = get_sub_field("content");
							 ?>
							 <div id="<?php echo 'c'. $i; ?>" role="tabpanel" class="interactive-overlay tab-pane fade">
							 	<div class="row">
							 		<div class="col-md-6 interactive-overlay-image"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></div>
							 		<div class="col-md-6 interactive-overlay-content">
							 			<h3><?php echo $title; ?></h3>
							 			<?php echo $content; ?>
							 		</div>
							 	</div>
							 	<a class="close-overlay" href="#">X</a>
							 </div>
						<?php endwhile; ?>
					</div>
					</svg>
				</div>
			</div>
		<?php endif; ?>
		</div>
	</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>