<?php

	switch ( get_row_layout()) {
		case 'get_why_us':
			get_template_part('/layouts/layouts/why-us');
		break;
		case 'short_services':
			get_template_part('/layouts/layouts/short-services');
		break;
		case 'current_openings':
			get_template_part('/layouts/layouts/current-openings');
		break;
		case 'perks_and_benefits':
			get_template_part('/layouts/layouts/perks-and-benefits');
		break;
		case 'interactive_content':
			get_template_part('/layouts/layouts/interactive-content');
		break;
		case 'testimonials':
			get_template_part('/layouts/layouts/testimonials');
		break;
	}

?>